.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


==============================
Le Filament - Tableaux de Bord
==============================

Affichage de tableaux de bord : 
 - suivi annuel Factures/Commandes/Pipe et Trésorerie
 - tableau des performances mensuelles
 - graphe de la tésorerie sur 1 an glissant
 - prévisionnel de la trésorerie sur 6 mois



Credits
=======

Contributors ------------

* Benjamin Rivier <benjamin@le-filament.com>
* Remi Cazenave <remi@le-filament.com>


Maintainer ----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament