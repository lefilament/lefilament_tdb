# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class res_company(models.Model):
    _inherit = "res.company"

    ca_target = fields.Integer("Objectif Chiffre d'Affaire")
    charges_fixes = fields.Integer('Charges Fixes')
    previ_treso_ids = fields.One2many('previ.treso',
                                      'company_id',
                                      'Prévisionnel')


class previ_treso(models.Model):
    _name = "previ.treso"
    _description = "Previsionnel de tresorerie"
    _order = 'name'

    company_id = fields.Many2one(
        'res.company',
        'Company',
        default=lambda self: self.env.user.company_id.id)
    name = fields.Char('Nom')
    periode = fields.Selection(
        [(12, 'Mensuel'), (3, 'Trimestriel'), (1, 'Annuel')],
        srting='Période'
        )
    date = fields.Date('Date')
    montant = fields.Float('Montant')
