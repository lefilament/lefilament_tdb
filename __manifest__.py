# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    'name': "Le Filament - Tableau de Bord",

    'summary': """
        Rapports Le Filament""",

    'description': """
        Tableaux de Bord Le Filament

        Affichage de tableaux de bord :
         - suivi annuel Factures/Commandes/Pipe et Trésorerie
         - tableau des performances mensuelles
         - graphe de la tésorerie sur 1 an glissant
         - prévisionnel de la trésorerie sur 6 mois

    """,
    'author': "LE FILAMENT",
    'category': 'dashboard',
    'website': "https://le-filament.com",
    'version': '12.0.1.0.0',
    'license': 'AGPL-3',
    'depends': ['account', 'crm', 'hr_expense', 'sale'],
    'data': [
        'security/lefilament_dashboard_security.xml',
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/views.xml',
        'views/schedule.xml',
        'data/ir_module_category.xml'
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
}
